export type Optional<T> = T | null;
export type PowerValue = { time: number, value: number }
export type ChartData = { x: number, y: number }
