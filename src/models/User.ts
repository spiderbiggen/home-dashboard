import {DatedEntity} from './DatedEntity';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
export interface User extends DatedEntity {
  username: string;
  email: string;
  password: string; // technically a hash of the password.
  roles?: string[];
}
