export type TokenResponse = {
  access_token: string;
  expires_in: string;
  refresh_token?: string;
  refresh_token_expires_in?: string;
}

export type Agreement = {
  agreementId: string;
  agreementIdChecksum: string;
  street: string;
  houseNumber: string;
  postalCode: string;
  city: string;
  heatingType: string;
  displayCommonName: string;
  displayHardwareVersion: string;
  displaySoftwareVersion: string;
  isToonSolar: boolean;
  isToonly: boolean;
}

export type FlowData = {
  timestamp: number;
  unit: string;
  value: number;
}

export enum Interval {
  HOURS = 'hours',
  DAYS = 'days',
  WEEKS = 'weeks',
  MONTHS = 'months',
  YEARS = 'years',
}

export enum EnergyType {
  ELECTRICITY = 'electricity',
  GAS = 'gas',
  DISTRICTHEAT = 'districtheat',
}

export enum ConsumptionType {
  FLOW = 'flows',
  DATA = 'Data',
}

export type LastUpdate = {
  lastUpdatedFromDisplay: number;
}

export type ThermostatState = {
  id: number;
  tempValue: number;
  dhw: number;
}

export type ThermostatInfo = LastUpdate & {
  currentSetpoint: number;
  currentDisplayTemp: number;
  programState: number;
  activeState: number;
  nextProgram: number;
  nextState: number;
  nextTime: number;
  nextSetpoint: number;
  hasBoilerFault: number;
  errorFound: number;
  boilerModuleConnected: number;
  realSetpoint: number;
  burnerInfo: string;
  otCommError: string;
  currentModulationLevel: number;
  haveOTBoiler: number;
}

export type Usage = LastUpdate & {
  installed?: number;
  value: number;
  avgValue: number;
  avgDayValue: number;
  meterReading: number;
  dayCost: number;
  dayUsage: number;
  isSmart: number;
}

export type PowerUsage = Usage & {
  valueProduced: number;
  valueSolar: number;
  maxSolar: number;
  avgProduValue: number;
  meterReadingLow: number;
  meterReadingProdu: number;
  meterReadingLowProdu: number;
  dayLowUsage: number;
  lowestDayValue: number;
  solarProducedToday: number;
}

export type Device = unknown;

export type Devices = LastUpdate & {
  device: Device[] | Device
}

export type DeviceStatusInfo = Devices & {
  inSwitchAllTotal: {
    currentState: 0;
    currentUsage: 0;
    dayUsage: 0;
    avgUsage: 0;
  };
}

export type StatusResponse = {
  thermostatStates: LastUpdate & {
    state: ThermostatState[];
  };
  thermostatInfo: ThermostatInfo;
  smokeDetectors: Devices;
  deviceConfigInfo: Devices;
  deviceStatusInfo: DeviceStatusInfo;
  powerUsage: PowerUsage;
  gasUsage: Usage;
  waterUsage: Usage;
  lastUpdateFromDisplay: number;
  serverTime: number;
}

export type FlowResponse = { [key in keyof Interval]?: FlowData[] }
