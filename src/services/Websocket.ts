import io from 'socket.io-client';

const SERVER_URL = process.env.NODE_ENV === 'production'
  ? 'https://api.spiderbiggen.com/'
  : 'http://localhost:3000/';
const PATH = process.env.NODE_ENV === 'production'
  ? '/api-home/socket.io'
  : undefined;

export const socket = io(SERVER_URL + 'toon', { autoConnect: false, path: PATH });
