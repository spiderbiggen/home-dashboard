import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { IStore } from '@/store/modules/IStore';
import { Optional, PowerValue } from '@/models/Types';
import { Agreement, PowerUsage, StatusResponse, ThermostatInfo, Usage } from '@/models/ToonResponseModels';
import HttpService from '@/services/HttpService';
import { ToonModule, UserModule } from '@/store';
import { socket } from '@/services/Websocket';
import { dropRepeatsWith, takeLast } from 'ramda';

@Module({ name: 'toon' })
export class ToonStore extends VuexModule implements IStore {
  status: Optional<StatusResponse> = null;
  agreements: Agreement[] = [];
  agreement: Optional<Agreement> = null;
  powerHistory: PowerValue[] = [];

  @Action
  async init () {
    if (UserModule.services.includes('TOON')) {
      await ToonModule.getUpdates();
      await ToonModule.subscribe();
    }
  }

  @Action
  async getUpdates () {
    await this.getStatus();
  }

  @Action
  async getAgreements () {
    try {
      const response = await HttpService.get<Agreement[]>('toon/agreements');
      ToonModule.setAgreements(response);
      if (ToonModule.agreements.length === 1) {
        ToonModule.setAgreement(ToonModule.agreements[0]);
      }
    } catch (e) {
      console.error(e);
    }
  }

  @Action
  async getStatus () {
    try {
      if (!this.agreement) {
        await this.getAgreements();
      }
      if (!this.agreement) {
        return false;
      }
      const agreementId = this.agreement.agreementId;
      const response = await HttpService.get<StatusResponse>(`toon/${agreementId}/status`);
      ToonModule.setStatus(response);
    } catch (e) {
      console.error(e);
    }
  }

  @Action({ rawError: true })
  async updateTemperature (obj: ThermostatInfo) {
    if (!this.agreement) {
      throw new Error('No agreement selected');
    }
    if (!obj) {
      throw new Error(`Bruh what`);
    }
    const status: Optional<StatusResponse> = ToonModule.status;
    if (!status) {
      return;
    }
    try {
      const agreementId = this.agreement.agreementId;
      status.thermostatInfo = await HttpService.put<ThermostatInfo>(`toon/${agreementId}/thermostat`, obj);
      status.thermostatInfo.activeState = -1;
      status.thermostatInfo.programState = 2;
      ToonModule.setStatus(status);
    } catch (e) {
      console.error(e);
    }
  }

  @Action({ rawError: true })
  async subscribe () {
    if (!this.agreement) {
      return;
    }
    if (socket.connected) return;

    await HttpService.post(`toon/${this.agreement.agreementId}/subscribe`);
    const group = this.agreement.displayCommonName;
    socket.on('connect', () => {
      socket.emit('join', group);
      socket.on('gas', ([gas]: [Usage]) => {
        console.log('gas');
        if (!ToonModule.status) return;
        ToonModule.status.gasUsage = gas;
        ToonModule.setStatus(ToonModule.status);
      });
      socket.on('thermostat', ([thermostat]: [ThermostatInfo]) => {
        console.log('thermostat');
        if (!ToonModule.status) return;
        ToonModule.status.thermostatInfo = thermostat;
        ToonModule.setStatus(ToonModule.status);
      });
      socket.on('power', ([power]: [PowerUsage]) => {
        console.log('power');
        if (!ToonModule.status) return;
        ToonModule.status.powerUsage = power;
        ToonModule.setStatus(ToonModule.status);
        ToonModule.addPowerHistory({ time: power.lastUpdatedFromDisplay, value: power.value });
      });
    });
    socket.connect();
  }

  @Mutation
  setStatus (status: Optional<StatusResponse>) {
    this.status = status || null;
  }

  @Mutation
  setAgreements (agreements: Agreement[]) {
    this.agreements = agreements;
  }

  @Mutation
  setAgreement (agreement: Optional<Agreement>) {
    this.agreement = agreement;
  }

  @Mutation
  addPowerHistory (history: PowerValue) {
    let arr = Array.from(this.powerHistory);
    arr.push(history);
    arr.sort((a, b) => a.time - b.time);
    arr = dropRepeatsWith((left, right) => left.time === right.time, arr);
    this.powerHistory = takeLast(20, arr);
  }

  @Mutation
  setPowerHistory (history: PowerValue[]) {
    this.powerHistory = history;
  }

  @Mutation
  clear (): void {
    if (socket.connected) {
      socket.disconnect();
    }
  }
}
