import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { IStore } from '@/store/modules/IStore';
import { ToonModule, UserModule } from '@/store';

@Module({ name: 'app' })
export class AppStore extends VuexModule implements IStore {

  @Action({ rawError: true })
  async start () {
    if (await UserModule.refreshToken()) {
      await Promise.all([
        UserModule.getSelf(),
        UserModule.getServices(),
      ]);

      await Promise.all([
        ToonModule.init(),
      ]);
    }
  }

  @Mutation
  clear (): void {
  }
}
