import { Mutation, VuexModule } from 'vuex-module-decorators';
import { UserModule } from '@/store';
import HttpService from '@/services/HttpService';

export abstract class IStore extends VuexModule {
  static clearAll () {
    UserModule.clear();
  }

  abstract clear (): void;
}
