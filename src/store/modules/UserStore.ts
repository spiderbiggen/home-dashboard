import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import HttpService from '@/services/HttpService';
import { IStore } from '@/store/modules/IStore';
import { UserModule } from '@/store';
import { TokenRequest } from '@/models/Requests';
import { Optional } from '@/models/Types';
import { User } from '@/models/User';
import { Services, Toon_Consumer_Key } from '@/constants/Services';

const CALLBACK_URL = process.env.NODE_ENV === 'production'
  ? 'https%3A%2F%2Fsub.spiderbiggen.com%2Fhome%2Ftoon%2Fcallback'
  : 'http%3A%2F%2Flocalhost%3A8080%2Ftoon%2Fcallback';

@Module({ name: 'user' })
export class UserStore extends VuexModule implements IStore {
  token: Optional<string> = null;
  refresh: Optional<string> = null;
  self: Optional<User> = null;
  services: string[] = [];

  get loggedIn () {
    return !!this.token;
  }

  @Action
  public async logout () {
    IStore.clearAll();
  }

  @Action({ rawError: true })
  public async authorize (payload: { email: string, password: string }) {
    try {
      const { token, refresh_token } = await HttpService.post<TokenRequest>('users/authenticate', payload);
      UserModule.setToken(token);
      if (refresh_token) {
        UserModule.setRefreshToken(refresh_token);
      }
    } catch (error) {
      if (error.response.status === 401) {
        IStore.clearAll();
      }
      throw error;
    }
  }

  @Action({ rawError: true })
  public async register (payload: { email: string, password: string, username: string }) {
    await HttpService.post<User>('users/register', payload);
    await UserModule.authorize(payload);
  }

  @Action({ rawError: true })
  public async getSelf () {
    try {
      const self = await HttpService.get<User>('users/me');
      UserModule.setSelf(self);
    } catch (e) {
      console.error(e);
    }
  }

  @Action({ rawError: true })
  public async getServices () {
    try {
      const services = await HttpService.get<string[]>('users/services');
      UserModule.setServices(services);
    } catch (e) {
      console.error(e);
    }
  }

  @Action({ rawError: true })
  public async refreshToken () {
    const refresh = UserModule.refresh;
    if (!refresh) return false;
    try {
      const { token, refresh_token } = await HttpService.post<TokenRequest>('users/refresh', { refresh: refresh });
      await UserModule.setToken(token);
      if (refresh_token) {
        await UserModule.setRefreshToken(refresh_token);
      }
      return true;
    } catch (error) {
      if (error.response.status === 401) {
        IStore.clearAll();
      }
    }
    return false;
  }

  @Action({ rawError: true })
  public async addService ({ service, code }: { service: string, code?: Optional<string> }) {
    if (!Services.includes(service)) {
      throw new Error('UNKNOWN_SERVICE');
    }
    switch (service) {
      case 'TOON': {
        await this.addToon(code);
      }
    }
  }

  @Action({ rawError: true })
  private async addToon (code: Optional<string> = null) {
    if (!code) {
      window.location.href = `https://api.toon.eu/authorize?response_type=code&redirect_uri=${CALLBACK_URL}&client_id=${Toon_Consumer_Key}&tenant_id=eneco`;// &issuer=identity.toon.eu`;
      return;
    }
    await HttpService.post('toon/token', { code: code });
    await UserModule.getServices();
  }

  @Mutation
  setToken (token: Optional<string>) {
    this.token = token;
  }

  @Mutation
  setRefreshToken (token: Optional<string>) {
    this.refresh = token;
  }

  @Mutation
  setSelf (self: Optional<User>) {
    this.self = self;
  }

  @Mutation
  setServices (services: string[]) {
    this.services = services;
  }

  @Mutation
  clear (): void {
    this.token = null;
    this.refresh = null;
    this.self = null;
    this.services = [];
  }
}
