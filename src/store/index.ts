import Vue from 'vue';
import Vuex, { Plugin } from 'vuex';
import VuexPersist from 'vuex-persist';

import { getModule } from 'vuex-module-decorators';
import { AppStore } from '@/store/modules/AppStore';
import { UserStore } from '@/store/modules/UserStore';
import { ToonStore } from '@/store/modules/ToonStore';

Vue.use(Vuex);

function isStorageSupported (storage: Storage = localStorage) {
  const testKey = 'test';
  try {
    storage.setItem(testKey, '1');
    storage.removeItem(testKey);
    return true;
  } catch (error) {
    return false;
  }
}

// type State = {
//   app: AppStore,
//   user: UserStore,
//   toon: ToonStore,
// };

let vuexLocalStorage;
const plugins: Plugin<any>[] = [];
if (isStorageSupported() || isStorageSupported(sessionStorage)) {
  vuexLocalStorage = new VuexPersist({
    key: 'home-dashboard',
    storage: isStorageSupported() ? localStorage : sessionStorage,
    // reducer: (state: State) => {
    //   return {
    //     user: state.user,
    //     toon: {
    //       agreement: state.toon.agreement,
    //     },
    //   };
    // },
  });
  plugins.push(vuexLocalStorage.plugin);
}

const config = {
  modules: {
    app: AppStore,
    user: UserStore,
    toon: ToonStore,
  },
  plugins: plugins,
};
const store = new Vuex.Store(config);
export const AppModule = getModule(AppStore, store);
export const UserModule = getModule(UserStore, store);
export const ToonModule = getModule(ToonStore, store);

export default store;
