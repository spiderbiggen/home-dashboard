FROM nginx:stable-alpine
RUN mkdir -p /var/log/nginx
RUN mkdir -p /var/www/html

COPY ./dist /var/www/html
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
